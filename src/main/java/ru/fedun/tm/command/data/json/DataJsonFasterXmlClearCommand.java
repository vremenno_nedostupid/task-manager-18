package ru.fedun.tm.command.data.json;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataJsonFasterXmlClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-clear-json-fx";
    }

    @Override
    public String description() {
        return "Clear data from json (fasterxml) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FASTERXML) CLEAR]");
        final File file = new File(DataConstant.FILE_JSON_FX);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public Role[] roles(){
        return new Role[]{Role.ADMIN};
    }

}
