package ru.fedun.tm.command.data.json;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public final class DataJsonJaxbSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-save-json-jb";
    }

    @Override
    public String description() {
        return "Save data to json (jax-b) file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (JAX-B) SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConstant.FILE_JSON_JB);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final JAXBContext context = JAXBContext.newInstance(Domain.class);
        final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain, file);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
