package ru.fedun.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-load-json-fx";
    }

    @Override
    public String description() {
        return "Load data from json (fasterxml) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FASTERXML) LOAD]");
        final String jsonData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON_FX)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(jsonData, Domain.class);
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        serviceLocator.getProjectService().load(domain.getProjects());
        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println();
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
