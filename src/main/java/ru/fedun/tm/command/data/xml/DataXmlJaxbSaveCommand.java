package ru.fedun.tm.command.data.xml;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.constant.DataConstant;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public final class DataXmlJaxbSaveCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-save-xml-jb";
    }

    @Override
    public String description() {
        return "Save data to xml (jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConstant.FILE_XML_JB);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        final JAXBContext context = JAXBContext.newInstance(Domain.class);
        final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, file);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
