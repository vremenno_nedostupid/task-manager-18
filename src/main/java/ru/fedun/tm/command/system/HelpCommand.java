package ru.fedun.tm.command.system;

import ru.fedun.tm.bootstrap.Bootstrap;
import ru.fedun.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command.name() + "- " + command.description());
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
