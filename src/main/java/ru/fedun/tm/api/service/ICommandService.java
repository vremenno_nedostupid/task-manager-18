package ru.fedun.tm.api.service;

import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommands();

}
