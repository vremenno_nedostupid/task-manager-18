package ru.fedun.tm.api.service;

import ru.fedun.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
