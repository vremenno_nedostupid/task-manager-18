package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.notfound.TaskNotFoundException;
import ru.fedun.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ICrudRepository<Task> {

    private final List<Task> tasks = new ArrayList<>();

    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> result = findAll(userId);
        tasks.removeAll(result);
    }

    @Override
    public void add(final String userId, Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void add(List<Task> tasks) {
        for (final Task task : tasks) add(task);
    }

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        tasks.remove(task);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return tasks.get(index);
    }

    @Override
    public Task findOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        for (final Task task : tasks) {
            if (title.equals(task.getTitle())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Task task = findOneByTitle(userId, title);
        if (task == null) throw new TaskNotFoundException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        remove(userId, task);
        return task;
    }

    @Override
    public void load(final Task... tasks) {
        clear();
        for (final Task task : tasks) add(task);
    }

    @Override
    public void load(final List<Task> tasks) {
        clear();
        add(tasks);
    }

    @Override
    public void clear(){
        tasks.clear();
    }

}
