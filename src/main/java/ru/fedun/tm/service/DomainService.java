package ru.fedun.tm.service;

import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.api.service.IDomainService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.dto.Domain;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.empty.EmptyDomainException;

public class DomainService implements IDomainService {

    private final ICrudService<Task> taskService;

    private final ICrudService<Project> projectService;

    private final IUserService userService;

    public DomainService(
            final ICrudService<Task> taskService,
            final ICrudService<Project> projectService,
            final IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
    }

}
